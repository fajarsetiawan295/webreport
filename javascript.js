
const firebaseConfig = {
  //   copy your firebase config informations
  apiKey: "AIzaSyCdZ-vFV5nYxyLnSDy2J_WpEuGhn3T2E1w",
  authDomain: "aplikasi-cabai.firebaseapp.com",
  databaseURL: "https://aplikasi-cabai-default-rtdb.firebaseio.com",
  projectId: "aplikasi-cabai",
  storageBucket: "aplikasi-cabai.appspot.com",
  messagingSenderId: "39258928851",
  appId: "1:39258928851:web:a31f296da77101461f4626",
  measurementId: "G-0YRL0LSXLR"
};

// initialize firebase
firebase.initializeApp(firebaseConfig);

// reference your database
var contactFormDB = firebase.database().ref("histori_data");

getFirebase();

function getFirebase() {
  firebase.database().ref('/histori_data').orderByChild('Time').on('value', snapshot => {
    let arrayPush = [];

    snapshot.forEach(child => {
      let timeSplit
      let dateSplit
      if (child.val().Time) {
        let parse = child.val().Time.split(' ');
        timeSplit = parse[0]
        dateSplit = parse[1]
      } else {
        timeSplit = '';
        dateSplit = '';
      }
      let arrayData = {
        key: child.key,
        Kelembapan_tanah: child.val().Kelembapan_tanah,
        Kipas: child.val().Kipas,
        Ph_Tanah: child.val().Ph_Tanah,
        Pompa_Air: child.val().Pompa_Air,
        Pompa_PH: child.val().Pompa_PH,
        Suhu_Udara: child.val().Suhu_Udara,
        Date: dateSplit,
        Time: timeSplit

      }
      arrayPush.push(arrayData);
    });
    KTApexChartsDemo.init(arrayPush);

  })
}


const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';

var KTApexChartsDemo = function () {
  var _demo2 = function (products) {

    const groupByCategory = products.reduce((group, product) => {
      const { Date } = product;
      group[Date] = group[Date] ?? [];
      group[Date].push(product);
      return group;
    }, {});
    let result = Object.keys(groupByCategory).map((key) => [key, groupByCategory[key]])

    console.log(result.length)

    // console.log(result);

    let dateArray = []
    let Kelembapan_tanah = []
    let Ph_Tanah = [];
    let Suhu_Udara = [];

    if (result.length >= 7) {
      result.forEach(dataMpas => {
        let kt = 0
        let ph = 0
        let su = 0
        dataMpas[1].forEach(dataLopp => {
          if (dataLopp.Kelembapan_tanah) {
            kt += dataLopp.Kelembapan_tanah
          }
          if (dataLopp.Ph_Tanah) {
            ph += dataLopp.Ph_Tanah
          }
          if (dataLopp.Suhu_Udara) {
            su += dataLopp.Suhu_Udara
          }
        })

        Kelembapan_tanah.push(Math.ceil(kt / dataMpas[1].length))
        Ph_Tanah.push(Math.ceil(ph / dataMpas[1].length))
        Suhu_Udara.push(Math.ceil(su / dataMpas[1].length))
        dateArray.push(dataMpas[0]);
      });
    }

    if (result.length < 7 && result.length > 3) {
      result.forEach(dataMpas => {

        let countData = 0;


        for (let dataLopp of dataMpas[1]) {

          let kt = 0
          let ph = 0
          let su = 0

          if (dataLopp.Kelembapan_tanah) {
            kt = dataLopp.Kelembapan_tanah
          }
          if (dataLopp.Ph_Tanah) {
            ph = dataLopp.Ph_Tanah
          }
          if (dataLopp.Suhu_Udara) {
            su = dataLopp.Suhu_Udara
          }

          if (countData == 3) {
            break;
          }

          Kelembapan_tanah.push(Math.ceil(kt))
          Ph_Tanah.push(Math.ceil(ph))
          Suhu_Udara.push(Math.ceil(su))

          dateArray.push(dataLopp.Date + ' ' + dataLopp.Time);
          countData += 1;
        }
      });
    }

    if (result.length <= 3) {
      result.forEach(dataMpas => {

        let countData = 0;


        for (let dataLopp of dataMpas[1]) {

          let kt = 0
          let ph = 0
          let su = 0

          if (dataLopp.Kelembapan_tanah) {
            kt = dataLopp.Kelembapan_tanah
          }
          if (dataLopp.Ph_Tanah) {
            ph = dataLopp.Ph_Tanah
          }
          if (dataLopp.Suhu_Udara) {
            su = dataLopp.Suhu_Udara
          }

          if (countData == 10) {
            break;
          }

          Kelembapan_tanah.push(Math.ceil(kt))
          Ph_Tanah.push(Math.ceil(ph))
          Suhu_Udara.push(Math.ceil(su))

          dateArray.push(dataLopp.Date + ' ' + dataLopp.Time);
          countData += 1;
        }
      });
    }



    // console.log(Ph_Tanah);

    const apexChart = "#chart_2";
    $('#chart_2').empty();

    var options = {
      series: [{
        name: 'Kelembapan Tanah',
        data: Kelembapan_tanah
      }, {
        name: 'Ph Tanah',
        data: Ph_Tanah
      },
      {
        name: 'Suhu Udara',
        data: Suhu_Udara
      }
      ],
      chart: {
        height: 400,
        type: 'area'
      },
      dataLabels: {
        enabled: false,
        font: {
          size: 12,
        }
      },
      xaxis: {
        type: 'string',
        categories: dateArray,
        labels: {
          style: {
            fontSize: '7px',
            fontWeight: 400,
          },
        }
      },
      stroke: {
        curve: 'smooth'
      },
      tooltip: {
      },
      legend: {
        display: false
      },
      colors: [primary, success, warning]
    };

    var chart = new ApexCharts(document.querySelector(apexChart), options);
    chart.render();
  }

  var _demo11 = function (database) {
    const apexChart = "#chart_11";
    $('#chart_11').empty();

    let kt = 0
    let ph = 0
    let su = 0
    let ktloop = 0
    let phloop = 0
    let suloop = 0

    database.forEach(dataLopp => {
      if (dataLopp.Kelembapan_tanah) {
        kt += dataLopp.Kelembapan_tanah
        ktloop += 1
      }
      if (dataLopp.Ph_Tanah) {
        ph += dataLopp.Ph_Tanah
        phloop += 1
      }
      if (dataLopp.Suhu_Udara) {
        su += dataLopp.Suhu_Udara
        suloop += 1
      }
    })

    kthasil = Math.ceil(kt / ktloop)
    phhasil = Math.ceil(ph / phloop)
    suhasil = Math.ceil(su / suloop)

    var options = {
      series: [kthasil, phhasil, suhasil],
      chart: {
        width: 380,
        type: 'donut',
      },
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 350
          },
          legend: {
            position: 'bottom'
          }
        }
      }],
      labels: ['Kelembapan Tanah', 'Ph Tanah', 'Suhu Udara'],
      colors: [primary, success, warning, danger, info]
    };

    var chart = new ApexCharts(document.querySelector(apexChart), options);
    chart.render();
  }

  return {
    // public functions
    init: function (data) {

      data.sort(function (a, b) {
        return parseFloat(a.Date) - parseFloat(b.Date);
      });

      let tglm = document.getElementById("tgl-mulai").value
      let tgla = document.getElementById("tgl-akhir").value
      if (tglm != '' && tgla != '') {
        let splitm = tgla.split('-')
        let splita = tglm.split('-')

        var range = {
          min: splita[2] + '/' + splita[1] + '/' + splita[0],
          max: splitm[2] + '/' + splitm[1] + '/' + splitm[0],
        };
        console.log(range);
        var data = data.filter(function (o) {
          return o.Date <= range.max && o.Date >= range.min;
        });

      }

      _demo2(data);
      _demo11(data);
    }
  }
}();


